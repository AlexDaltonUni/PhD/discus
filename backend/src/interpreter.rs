use crate::fragmenter::{Program, Fragment, LocalInstruction, Operation, Operand, Data};
use crate::cryptosystems::{lfhe::LFHE, bfv::BFVCipherText, bfv::BFV};

pub fn interpret(program: &mut Program) -> Result<(), &str> {
    for i in 0..program.len() {
        if !program[i].done() && program[i].ready() {
            execute_fragment(&mut program[i])?;
            propegate_results(i, program)?;
        }
    }

    for fragment in program {
        if !fragment.done() {
            return Result::Err("not all fragments executed");
        }
    }

    Result::Ok(())
}

pub fn propegate_results(index: usize, program: &mut Program) -> Result<(), &'static str> {
    let completed_fragment = program[index].clone();
    for i in completed_fragment.instructions() {
        for r in i.results() {
            for f in (index + 1)..program.len() {
                program[f].operands_mut().iter_mut().filter(|o| o.id() == completed_fragment.operands()[*r].id())
                    .for_each(|o| o.set_data(completed_fragment.operands()[*r].data().clone()));
            }
        }
    }

    Result::Ok(())
}

pub fn replace_operands(fragment: &mut Fragment, operands: &Vec<Operand>) -> Result<(), &'static str> {
    for o in operands {
        fragment.operands_mut().iter_mut().filter(|x| x.id() == o.id())
            .for_each(|x| x.set_data(o.data().clone()));
    }

    Result::Ok(())
}

pub fn execute_fragment(fragment: &mut Fragment) -> Result<(), &'static str> {
    for i in 0..fragment.instructions().len() {
        match fragment.instructions()[i].operation() {
            Operation::Add | Operation::Sub | Operation::Mul | Operation::Div => {
                let result = execute_arithmetic(&fragment.instructions()[i], fragment.operands())?;
                let index = fragment.instructions()[i].results()[0];
                fragment.update(index, result)?;
            },
            Operation::LessThan | Operation::Equal => {
                return Result::Err("Not implemented comparison instructions");
            }
            Operation::Enc => {
                let result = execute_enc(&fragment.instructions()[i], fragment.operands())?;
                let index = fragment.instructions()[i].results()[0];
                fragment.update(index, result)?;
            }
            Operation::Dec => {
                let result = execute_dec(&fragment.instructions()[i], fragment.operands())?;
                let index = fragment.instructions()[i].results()[0];
                fragment.update(index, result)?;
            }
        }
    }

    fragment.finished();
    Result::Ok(())
}

fn execute_enc(i: &LocalInstruction, data: &Vec<Operand>) -> Result<Data, &'static str> {
    if let Data::Int(m) = data[i.operands()[0]].data() {
        if let Data::PublicKey(p, pk) = data[i.operands()[1]].data() {
            let mut bfv = BFV::new(p);
            let mut m_bfv = bfv.plaintext();
            m_bfv.pack(0, *m);
            let c = BFVCipherText::enc(&m_bfv, p, pk)?;
            return Result::Ok(Data::CipherText(c));
        }
    }
    
    Result::Err("Execute Enc: Wrong operands")
}

fn execute_dec(i: &LocalInstruction, data: &Vec<Operand>) -> Result<Data, &'static str> {
    if let Data::CipherText(c) = data[i.operands()[0]].data() {
        if let Data::SecretKey(sk) = data[i.operands()[1]].data() {
            let m = match c.dec(sk)?.unpack(0) {
                Some(i) => i,
                None => { return Result::Err("m larger than 64 bits"); }
            };
            return Result::Ok(Data::Int(m));
        }
    }
    
    Result::Err("Execute Dec: Wrong operands")
}
fn plaintext_operands(i: &LocalInstruction, data: &Vec<Operand>) -> bool {
    for op in i.operands() {
        match data[*op].data() {
            Data::CipherText(..) => { return false; },
            _ => { continue; }
        }
    }

    true
}

fn ciphertext_operands(i: &LocalInstruction, data: &Vec<Operand>) -> bool {
    for op in i.operands() {
        match data[*op].data() {
            Data::Int(..) => { return false; },
            _ => { continue; }
        }
    }

    true
}

fn match_encryption(i: &LocalInstruction, data: &Vec<Operand>) -> Result<(LocalInstruction, Vec<Operand>), &'static str> {
    let mut data = data.clone();
    let mut operands = i.operands().clone();
    if let Data::CipherText(a) = data[i.operands()[0]].data() {
        let params = a.parameters();
        let mut bfv = BFV::new(&params);
        let b = match data[i.operands()[1]].data() {
            Data::Int(i) => BFVCipherText::enc(&bfv.plaintext_from(*i), params, a.pk())?,
            _ => { return Result::Err("non-integer other-operand"); }
        };
        operands[1] = data.len();
        data.push(Operand::new(0, Data::CipherText(b), data[i.operands()[1]].local()));
    } else if let Data::CipherText(a) = data[i.operands()[1]].data() {
        let params = a.parameters();
        let mut bfv = BFV::new(&params);
        let b = match data[i.operands()[0]].data() {
            Data::Int(i) => BFVCipherText::enc(&bfv.plaintext_from(*i), params, a.pk())?,
            _ => { return Result::Err("non-integer other-operand"); }
        };
        operands[0] = data.len();
        data.push(Operand::new(0, Data::CipherText(b), data[i.operands()[0]].local()));
    }

    Result::Ok((
            LocalInstruction::new(i.operation(), operands, i.results().clone()),
            data
        ))
}

fn execute_arithmetic(i: &LocalInstruction, data: &Vec<Operand>) -> Result<Data, &'static str> {
    if plaintext_operands(i, data) {
        return execute_plaintext_arithmetic(i, data);
    } else if ciphertext_operands(i, data) {
        return execute_ciphertext_arithmetic(i, data);
    } else {
        let (new_i, new_data) = match_encryption(i, data)?;
        return execute_ciphertext_arithmetic(&new_i, &new_data);
    }
}

fn execute_plaintext_arithmetic(i: &LocalInstruction, data: &Vec<Operand>) -> Result<Data, &'static str> {
    if let Data::Int(a) = data[i.operands()[0]].data() {
        if let Data::Int(b) = data[i.operands()[1]].data() {
            return match i.operation() {
                Operation::Add => Result::Ok(Data::Int(a + b)),
                Operation::Sub => Result::Ok(Data::Int(a - b)),
                Operation::Mul => Result::Ok(Data::Int(a * b)),
                Operation::Div => Result::Ok(Data::Int(a / b)),
                _ => Result::Err("unrecognised arithmetic operation")
            }
        }
    }
    Result::Err("Operands not plaintexts")
}

fn execute_ciphertext_arithmetic(i: &LocalInstruction, data: &Vec<Operand>) -> Result<Data, &'static str> {
    if let Data::CipherText(a) = data[i.operands()[0]].data() {
        if let Data::CipherText(b) = data[i.operands()[1]].data() {
            if a.parameters() != b.parameters() { return Result::Err("ciphertexts do not match") }
            return match i.operation() {
                Operation::Add => Result::Ok(Data::CipherText(a + b)),
                Operation::Sub => Result::Ok(Data::CipherText(a - b)),
                Operation::Mul => Result::Ok(Data::CipherText(a * b)),
                _ => Result::Err("unimplemented arithmetic operation")
            }
        }
    }
    Result::Err("Operands not ciphertexts")
}

//TODO: write commit_completed_fragment function for Program


#[cfg(test)]
mod test {
    use super::*;
    use crate::fragmenter::*;
    use crate::cryptosystems::lfhe::LFHE;
    use crate::cryptosystems::bfv::BFVCipherText;

    #[test]
    fn execute_plaintext_arithmetic_test() {
        let mut state = State::new();
        let a = state.gen_operand(Data::Int(6), false);
        let b = state.gen_operand(Data::Int(2), false);
        let data = vec![a, b];

        let i_add = vec![state.gen_instruction(Operation::Add, data.clone())];
        let i_sub = vec![state.gen_instruction(Operation::Sub, data.clone())];
        let i_mul = vec![state.gen_instruction(Operation::Mul, data.clone())];
        let i_div = vec![state.gen_instruction(Operation::Div, data.clone())];

        let mut f_add = state.gen_fragment(&i_add);
        let mut f_sub = state.gen_fragment(&i_sub);
        let mut f_mul = state.gen_fragment(&i_mul);
        let mut f_div = state.gen_fragment(&i_div);

        execute_fragment(&mut f_add).unwrap();
        execute_fragment(&mut f_sub).unwrap();
        execute_fragment(&mut f_mul).unwrap();
        execute_fragment(&mut f_div).unwrap();

        assert_eq!(*f_add.operands()[2].data(), Data::Int(8));
        assert_eq!(*f_sub.operands()[2].data(), Data::Int(4));
        assert_eq!(*f_mul.operands()[2].data(), Data::Int(12));
        assert_eq!(*f_div.operands()[2].data(), Data::Int(3));
    }

    #[test]
    fn execute_ciphertext_arithmetic_test() {
        let mut state = State::new();
        let mut crypto = state.gen_cryptosystem(8, 1);
        let keys = crypto.keygen().unwrap();
        let mut a_m = crypto.plaintext();
        a_m.pack(0, 6);
        let mut b_m = crypto.plaintext();
        b_m.pack(0, 2);
        let a = state.gen_operand(Data::CipherText(BFVCipherText::enc(&a_m, crypto.parameters(), &keys.1).unwrap()), false);
        let b = state.gen_operand(Data::CipherText(BFVCipherText::enc(&b_m, crypto.parameters(), &keys.1).unwrap()), false);
        let data = vec![a, b];

        println!("a_m: {:?}", a_m);
        println!("b_m: {:?}", b_m);

        let i_add = vec![state.gen_instruction(Operation::Add, data.clone())];
        let i_sub = vec![state.gen_instruction(Operation::Sub, data.clone())];
        let i_mul = vec![state.gen_instruction(Operation::Mul, data.clone())];

        let mut f_add = state.gen_fragment(&i_add);
        let mut f_sub = state.gen_fragment(&i_sub);
        let mut f_mul = state.gen_fragment(&i_mul);

        execute_fragment(&mut f_add).unwrap();
        execute_fragment(&mut f_sub).unwrap();
        execute_fragment(&mut f_mul).unwrap();

        let r_add = match f_add.operands()[2].data() {
            Data::CipherText(c) => c,
            _ => { panic!("not a ciphertext"); }
        };
        let r_sub = match f_sub.operands()[2].data() {
            Data::CipherText(c) => c,
            _ => { panic!("not a ciphertext"); }
        };
        let r_mul = match f_mul.operands()[2].data() {
            Data::CipherText(c) => c,
            _ => { panic!("not a ciphertext"); }
        };

        println!("r_add: {:?}", r_add.dec(&keys.0).unwrap());
        println!("r_sub: {:?}", r_sub.dec(&keys.0).unwrap());
        println!("r_mul: {:?}", r_mul.dec(&keys.0).unwrap());
        assert_eq!(r_add.dec(&keys.0).unwrap().unpack(0), Some(8));
        assert_eq!(r_sub.dec(&keys.0).unwrap().unpack(0), Some(4));
        assert_eq!(r_mul.dec(&keys.0).unwrap().unpack(0), Some(12));
    }

    #[test]
    fn interpret_test() {
        let mut state = State::new();
        let a = state.gen_operand(Data::Int(6), false);
        let b = state.gen_operand(Data::Int(2), false);
        let data_1 = vec![a, b];
        let i_1 = state.gen_instruction(Operation::Add, data_1);
        let wait_op = i_1.get_wait_op().unwrap();
        let frag_1 = state.gen_fragment(&vec![i_1]);

        let c = state.gen_operand(Data::Int(5), false);
        let data_2 = vec![c, wait_op];
        let i_2 = state.gen_instruction(Operation::Add, data_2);
        let frag_2 = state.gen_fragment(&vec![i_2]);
        let mut program = vec![frag_1, frag_2];

        interpret(&mut program).unwrap();

        assert_eq!(program[1].operands()[2].data(), &Data::Int(13));
    }

    #[test]
    fn enc_dec_test() {
        let mut state = State::new();
        let mut bfv = state.gen_cryptosystem(8, 1);
        let (sk, pk) = bfv.keygen().unwrap();
        let params = bfv.parameters().clone();
        let a = state.gen_operand(Data::Int(6), false);
        let op_pk = state.gen_operand(Data::PublicKey(params.clone(), pk), false);
        let op_sk = state.gen_operand(Data::SecretKey(sk), true);
        let data_1 = vec![a, op_pk];
        let i_1 = state.gen_instruction(Operation::Enc, data_1);
        let i_2 = state.gen_instruction(Operation::Dec, vec![i_1.get_wait_op().unwrap(), op_sk]);
        let mut frag = state.gen_fragment(&vec![i_1, i_2]);

        execute_fragment(&mut frag).unwrap();

        assert_eq!(frag.operands()[4].data().clone(), Data::Int(6));
    }
}
