use rug::{rand::RandState, Integer};
use std::vec::Vec;
use std::time::SystemTime;
use serde::{Serialize, Deserialize};

//----------------------------------------------------------
// Generic element of a polynomial ring
//----------------------------------------------------------
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct RingElement {
    values: Vec<Integer>,
}

impl RingElement {
	pub fn from(values: Vec<Integer>) -> Self {
        RingElement { values }
	}

    pub fn values(&self) -> Vec<Integer> {
        self.values.clone()
    }

    pub fn set(&mut self, index: usize, value: Integer) {
        self.values[index] = value;
    }

    pub fn get(&self, index: usize) -> Integer { self.values[index].clone() }
}

//----------------------------------------------------------
// Generic polynomial ring
//----------------------------------------------------------
pub struct Ring {
    z: Integer,                         // Coeficient Modulus
    n: usize,                           // Ring order (polynomial ring modulo x^n + 1)
    log_n: u64,                         // Number of bits required to express n
    rand_state: RandState<'static>,     // Random number generator
}

impl Ring {
    // Constructor
    pub fn new(z: Integer, log_n: u64) -> Self {
        let mut rand_state = RandState::new();
        rand_state.seed(&Integer::from(SystemTime::now().elapsed().unwrap().as_secs()));
        Ring {
            z,
            log_n,
            n: 1 << log_n,
            rand_state,
        }
    }

    // Generates a uniformally random sample element from the ring
    pub fn sample(&mut self) -> RingElement {
        let mut values: Vec<Integer> = Vec::new();

        for _i in 0..self.n {
            values.push(Integer::from(self.z.clone()).random_below(&mut self.rand_state));
        }

        RingElement::from(values)
    }

    pub fn random_coefficient(&mut self) -> Integer {
        self.z.clone().random_below(&mut self.rand_state)
    }

    // Getter for the ring order
    pub fn order(&self) -> usize {
        self.n
    }

    pub fn log_n(&self) -> u64 {
        self.log_n
    }

    pub fn modulus(&self) -> Integer {
        self.z.clone()
    }

    // Polynomial addition within the ring, add(a, b) = a + b
    pub fn add(&self, a: &RingElement, b: &RingElement) -> RingElement {
        let values = poly_add(&a.values, &b.values);
        self.reduce(&values)
    }

    // Polynomial subtraction within the ring, sub(a, b) = a - b
    pub fn sub(&self, a: &RingElement, b: &RingElement) -> RingElement {
        let values = poly_sub(&a.values, &b.values);
        self.reduce(&values)
    }

    // Polynomial multiplication within the ring, mul(a, b) = a * b
    pub fn mul(&self, a: &RingElement, b: &RingElement) -> RingElement {
        let values = poly_mul(&a.values, &b.values);
        self.reduce(&values)
    }

    // Reduces a non-ring element to the element it is congruent with from the ring
    // a == reduce(a) Z[X]/(X^n + 1)
    pub fn reduce(&self, a: &Vec<Integer>) -> RingElement {
        let mut values: Vec<Integer> = Vec::new();

        for i in 0..a.len() {
            if i < self.n {
                values.push(rug_mod(a[i].clone(), self.z.clone()));
            } else {
                values[i % self.n] = rug_mod(values[i % self.n].clone() - a[i].clone(), self.z.clone());
            }
        }

        RingElement::from(values)
    }

    // Shifts ring elements into the ring modulo [q/2, q/2)
    pub fn q_shift(&self, a: &RingElement) -> RingElement {
        let (half_mod, ..) = self.z.clone().div_rem_ceil(Integer::from(2));
        let mut result = Vec::new();

        for i in 0..a.values.len() {
            result.push(a.values[i].clone() - half_mod.clone())
        }

        RingElement::from(result)
    }

    // Polynomial division within the ring div(a, b) = (quotient, remainder) s.t. b * quotient + remainder = a
    // the remainder produced has a smaller degree than b
    pub fn div(&self, a: &RingElement, b: &RingElement) -> Result<(RingElement, RingElement), &'static str> {
        let mut r = a.clone();
        let mut q = RingElement::from(vec![Integer::from(0); self.n]);

        let n = degree(&b.values);
        let u = match Integer::from(&b.values[n]).invert(&self.z) {
            Err(_e) => return Err("unable to invert operand"),
            Ok(r) => r,
        };

        if !zero(&b.values) {
            while (n > 0 && degree(&r.values) >= n) || (n == 0 && !zero(&r.values)) {
                let d = degree(&r.values);
                let mut v = RingElement::from(vec![Integer::from(0); self.n]);
                v.values[(d - n)] = rug_mod(Integer::from(&u) * Integer::from(&r.values[d]), self.z.clone());

                q = self.add(&q, &v);
                v = self.mul(&v, &b);
                r = self.sub(&r, &v);
            }
        }

        Ok((q, r))
    }

    // The extended Euclidean algorithm for ring elements extended_euclidean(a, b) = (s, t, r) s.t.
    // r is the GCD of a and b and r = s * a + t * b
    fn extended_euclidean(&self, a: &RingElement, b: &RingElement) -> Result<(RingElement, RingElement, RingElement), &'static str> {
        let mut r = b.clone();
        let mut old_r = a.clone();
        let mut s = RingElement::from(vec![Integer::from(0); self.n]);
        let mut old_s = RingElement::from(vec![Integer::from(0); self.n]);
        old_s.values[0] = Integer::from(1);
        let mut t = old_s.clone();
        let mut old_t = RingElement::from(vec![Integer::from(0); self.n]);

        while !zero(&r.values) {
            let (quotient, ..) = match self.div(&old_r, &r) {
                Err(..) => return Err("unable to divide"),
                Ok(r) => r,
            };

            let mut scratch = r.clone();
            r = self.sub(&old_r, &self.mul(&quotient, &r));
            old_r = scratch;

            scratch = s.clone();
            s = self.sub(&old_s, &self.mul(&quotient, &s));
            old_s = scratch;

            scratch = t.clone();
            t = self.sub(&old_t, &self.mul(&quotient, &t));
            old_t = scratch;
        }

        Ok((old_s, old_t, old_r))
    }

    // Inverts an element in the ring s.t. invert(a) * a = 1
	pub fn invert(&self, a: &RingElement) -> Result<RingElement, &'static str> {
        let mut b = vec![Integer::from(0); self.n + 1];
        b[0] = Integer::from(1);
        b[self.n] = Integer::from(1);

        let (u, _, mut d) = match self.extended_euclidean(a, &RingElement::from(b)) {
            Err(e) => return Err(e),
            Ok(r) => r,
        };

        if degree(&d.values) == 0 {
            d.values[0] = match Integer::from(&d.values[0]).invert(&self.z) {
                Err(..) => return Err("Could not invert"),
                Ok(r) => r,
            };
            let a_inv = self.mul(&d, &u);
            return Ok(a_inv);
        }

        Err("Could not invert")
    }

    pub fn additive_invert(&self, a: &RingElement) -> RingElement {
        let mut val = a.values();

        for i in 0..val.len() {
            val[i] = rug_mod(0 - val[i].clone(), self.z.clone());
        }

        RingElement::from(val)
    }
}

//----------------------------------------------------------
// Polynomial helper functions
//----------------------------------------------------------

// poly_mul(a, b) = a * b
pub fn poly_mul(a: &Vec<Integer>, b: &Vec<Integer>) -> Vec<Integer> {
    let mut values = vec![Integer::from(0); a.len() + b.len()];

    for i in 0..a.len() {
        for j in 0..b.len() {
            values[i+j] = values[i+j].clone() + a[i].clone() * b[j].clone();
        }
    }

    values
}

// poly_add(a, b) = a + b
pub fn poly_add(a: &Vec<Integer>, b: &Vec<Integer>) -> Vec<Integer> {
    let max_size = match a.len() > b.len() {
        true => a.len(),
        false => b.len(),
    };

    let mut values = vec![Integer::from(0); max_size];

    for i in 0..max_size {
        if i < a.len() { values[i] = values[i].clone() + a[i].clone(); }
        if i < b.len() { values[i] = values[i].clone() + b[i].clone(); }
    }

    values
}

// poly_sub(a, b) = a - b
pub fn poly_sub(a: &Vec<Integer>, b: &Vec<Integer>) -> Vec<Integer> {
    let max_size = match a.len() > b.len() {
        true => a.len(),
        false => b.len(),
    };
    let mut values = vec![Integer::from(0); max_size];

    for i in 0..max_size {
        if i < a.len() { values[i] = values[i].clone() + a[i].clone(); }
        if i < b.len() { values[i] = values[i].clone() - b[i].clone(); }
    }

    values
}

// Mathametical modulus function on abitrarily large integers
pub fn rug_mod(a: Integer, modulus: Integer) -> Integer {
    ((Integer::from(a) % Integer::from(&modulus)) + Integer::from(&modulus)) % modulus
}

// true iff a is the zero vector
pub fn zero(a: &Vec<Integer>) -> bool {
    for i in 0..a.len() {
        if a[i] != 0 { return false; }
    }

    true
}

// returns the index of the highest non-zero element in the vector, does not work
// if vector is the zero vector
pub fn degree(a: &Vec<Integer>) -> usize {
    let mut degree = 0;
    for i in 0..a.len() {
        if a[i] != 0 {
            degree = i;
        }
    }

    degree
}


#[cfg(test)]
mod test {
    use super::*;
    use rug::Integer;
    use rug::integer::IsPrime;
    use rand::RngCore;

    fn generate_test_prime() -> Integer {
        let mut rng = rand::thread_rng();
        let mut prime = rng.next_u32();
        while Integer::from(prime).is_probably_prime(15) == IsPrime::No {
            prime = rng.next_u32();
        }

        Integer::from(prime)
    }

    #[test]
    fn add_test() {
        let mut ring = Ring::new(Integer::from(64), 4);
        let a = ring.sample();
        println!("a: {:?}", a);
        let b = ring.sample();
        println!("b: {:?}", b);
        let c = ring.add(&a, &b);
        println!("c: {:?}", c);

        for i in 0..c.values.len() {
            assert!(c.values[i] < ring.z);
            assert_eq!(c.values[i], (a.values[i].clone() + b.values[i].clone()) % Integer::from(64));
        }
    }

    #[test]
    fn sub_test() {
        let mut ring = Ring::new(Integer::from(64), 4);
        let a = ring.sample();
        println!("a: {:?}", a);
        let b = ring.sample();
        println!("b: {:?}", b);
        let c = ring.sub(&a, &b);
        println!("c: {:?}", c);

        for i in 0..c.values.len() {
            assert_eq!(c.values[i], rug_mod(a.values[i].clone() - b.values[i].clone(), Integer::from(64)));
        }

        let a_prime = ring.add(&c, &b);
        
        assert_eq!(a_prime, a);
    }

    #[test]
    fn mul_test() {
        let mut ring = Ring::new(Integer::from(64), 4);
        let a = ring.sample();
        println!("a: {:?}", a);
        let b = ring.sample();
        println!("b: {:?}", b);
        let c = ring.mul(&a, &b);
        println!("c: {:?}", c);

        let mut output = vec![Integer::from(0); a.values.len() + b.values.len()];

        for i in 0..a.values.len() {
            for j in 0..b.values.len() {
                output[i+j] = Integer::from(&output[i+j])
                    + Integer::from(&a.values[i]) * Integer::from(&b.values[j]);
            }
        }

        let check = ring.reduce(&output);

        for i in 0..c.values.len() {
            assert_eq!(c.values[i], check.values[i]);
        }
    }

    #[test]
    fn div_test() {
        let modulus = generate_test_prime();
        let mut ring = Ring::new(Integer::from(&modulus), 2);
        let a = ring.sample();
        println!("a: {:?}", a);
        let b = ring.sample();
        println!("b: {:?}", b);

        let (quotient, remainder) = ring.div(&a, &b).expect("unable to divide operands");
        println!("modulus: {:?}", modulus);
        println!("quotient: {:?}", quotient);
        println!("remainder: {:?}", remainder);
        let mut check = ring.mul(&quotient, &b);
        println!("quotient * b: {:?}", check);
        check = ring.add(&check, &remainder);
        println!("quotient * b + remainder: {:?}", check);

        assert_eq!(check.values, a.values);
        assert!(degree(&remainder.values) < degree(&b.values));
    }

    #[test]
    fn constant_div_test() {
        let modulus = generate_test_prime();
        let mut ring = Ring::new(Integer::from(&modulus), 2);
        let a = ring.sample();
        println!("a: {:?}", a);
        let mut b = ring.sample();
        for i in 1..4 {
            b.values[i] = Integer::from(0);
        }
            b.values[0] = Integer::from(2);
        println!("b: {:?}", b);

        let (quotient, remainder) = ring.div(&a, &b).expect("unable to divide operands");
        println!("modulus: {:?}", modulus);
        println!("quotient: {:?}", quotient);
        println!("remainder: {:?}", remainder);
        let mut check = ring.mul(&quotient, &b);
        println!("quotient * b: {:?}", check);
        check = ring.add(&check, &remainder);
        println!("quotient * b + remainder: {:?}", check);

        assert_eq!(check.values, a.values);
    }

    #[test]
    fn extended_euclidean_test() {
        let modulus = generate_test_prime();
        let mut ring = Ring::new(Integer::from(modulus), 2);
        let a = ring.sample();
        println!("a: {:?}", a);
        let b = ring.sample();
        println!("b: {:?}", b);

        let (u, v, d) = ring.extended_euclidean(&a, &b).expect("unable to do extended euclidean algorithm");
        println!("u: {:?}", u);
        println!("v: {:?}", v);
        println!("d: {:?}", d);

        let ua = ring.mul(&a, &u);
        println!("ua: {:?}", ua);
        let vb = ring.mul(&b, &v);
        println!("vb: {:?}", vb);
        let ua_vb = ring.add(&ua, &vb);
        println!("ua_vb: {:?}", ua_vb);

        assert_eq!(ua_vb.values, d.values);
    }

    #[test]
    fn invert_test() {
        let modulus = generate_test_prime();
        let mut ring = Ring::new(Integer::from(modulus), 4);
        let a = ring.sample();
        println!("a: {:?}", a);
        let a_prime = ring.invert(&a).expect("unable to invert");
        println!("a_prime: {:?}", a_prime);
        let a_a_prime = ring.mul(&a, &a_prime);
        println!("a * a_prime: {:?}", a_a_prime);

        assert_eq!(a_a_prime.values[0], Integer::from(1));
        for i in 1 ..a_a_prime.values.len() {
            assert_eq!(a_a_prime.values[i], Integer::from(0));
        }
    }

    #[test]
    fn additive_invert_test() {
        let modulus = generate_test_prime();
        let mut ring = Ring::new(Integer::from(modulus), 4);
        let a = ring.sample();
        let a_inv = ring.additive_invert(&a);
        let z = ring.add(&a, &a_inv);
        assert!(zero(&z.values()));
    }
}
