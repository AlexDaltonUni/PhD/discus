use crate::cryptosystems::ring::RingElement;
use std::ops;
use rug::Integer;
use serde::{Serialize, Deserialize};

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct DoubleCRT {
    partials: Vec<DoubleCRTPartial>,
}

impl DoubleCRT {
    pub fn from_ring_element(source: &RingElement, modulus_products: &Vec<u64>) -> Self {
        let mut partials = Vec::new();

        for i in 0..modulus_products.len() {
            let mut values = Vec::new();
            for j in 0..source.values().len() {
                values.push((source.values()[j].clone() % modulus_products[i]).to_u64()
                    .expect("unable to convert to u64"));
            }
            partials.push(DoubleCRTPartial { values, modulus: modulus_products[i] } );
        }

        DoubleCRT{ partials }
    }

    pub fn from_partials(source: &Vec<DoubleCRTPartial>) -> Self {
        DoubleCRT { partials: source.to_vec() }
    }

    pub fn to_ring_element(&self) -> RingElement {
        let mut values = vec![Integer::from(0); self.partials[0].values.len()];
        let mut modulus = Integer::from(1);
        for i in 0..self.partials.len() {
            modulus = modulus * self.partials[i].modulus;
        }

        for i in 0..self.partials.len() {
            let q_factor = Integer::from(self.partials[i].modulus);
            let quotient = modulus.clone() / q_factor.clone();
            let inv = quotient.clone().invert(&q_factor).expect("unable to invert");

            for j in 0..self.partials[0].values.len() {
                let mut x = inv.clone() * self.partials[i].values[j];
                x = x % q_factor.clone();
                x = x * quotient.clone();
                x = x + values[j].clone();
                values[j] = x % modulus.clone();
            }
        }

        RingElement::from(values)
    }

    pub fn partials(&self) -> &Vec<DoubleCRTPartial> { &self.partials }
}

impl ops::Add<&DoubleCRT> for &DoubleCRT {
    type Output = DoubleCRT;
    fn add(self, rhs: &DoubleCRT) -> DoubleCRT {
        let mut partials = Vec::new();
        for i in 0..self.partials.len() {
            partials.push(&self.partials[i] + &rhs.partials[i]);
        }

        DoubleCRT{ partials }
    }
}

impl ops::Sub<&DoubleCRT> for &DoubleCRT {
    type Output = DoubleCRT;
    fn sub(self, rhs: &DoubleCRT) -> DoubleCRT {
        let mut partials = Vec::new();
        for i in 0..self.partials.len() {
            partials.push(&self.partials[i] - &rhs.partials[i]);
        }

        DoubleCRT{ partials }
    }
}

impl ops::Mul<&DoubleCRT> for &DoubleCRT {
    type Output = DoubleCRT;
    fn mul(self, rhs: &DoubleCRT) -> DoubleCRT {
        let mut partials = Vec::new();
        for i in 0..self.partials.len() {
            partials.push(&self.partials[i] * &rhs.partials[i]);
        }

        DoubleCRT{ partials }
    }
}

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct DoubleCRTPartial {
    values: Vec<u64>,
    modulus: u64,
}

impl ops::Add<&DoubleCRTPartial> for &DoubleCRTPartial {
    type Output = DoubleCRTPartial;
    fn add(self, rhs: &DoubleCRTPartial) -> DoubleCRTPartial {
        let mut values = Vec::new();
        for i in 0..self.values.len() {
            values.push((self.values[i] + rhs.values[i]) % self.modulus);
        }

        DoubleCRTPartial{ values, modulus: self.modulus }
    }
}

impl ops::Sub<&DoubleCRTPartial> for &DoubleCRTPartial {
    type Output = DoubleCRTPartial;
    fn sub(self, rhs: &DoubleCRTPartial) -> DoubleCRTPartial {
        let mut values = Vec::new();
        for i in 0..self.values.len() {
            values.push((self.modulus + self.values[i] - rhs.values[i]) % self.modulus);
        }

        DoubleCRTPartial{ values, modulus: self.modulus }
    }
}

impl ops::Mul<&DoubleCRTPartial> for &DoubleCRTPartial {
    type Output = DoubleCRTPartial;
    fn mul(self, rhs: &DoubleCRTPartial) -> DoubleCRTPartial {
        let mut values = vec![0; self.values.len()];
        for i in 0..self.values.len() {
            for j in 0..rhs.values.len() {
                if (i + j) < self.values.len() {
                    values[i+j] = (values[i+j] + (self.values[i] * rhs.values[j]
                        % self.modulus)) % self.modulus;
                } else {
                    values[(i+j) % self.values.len()] = (values[(i+j) % self.values.len()] + self.modulus
                        - (self.values[i] * rhs.values[j]) % self.modulus) % self.modulus;
                }
            }
        }

        DoubleCRTPartial{ values, modulus: self.modulus }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::cryptosystems::ring::Ring;
    use rand::Rng;
    use rand::RngCore;
    use rug::Integer;
    use rug::integer::IsPrime;

    fn generate_test_prime() -> u64 {
        let mut rng = rand::thread_rng();
        let mut prime = rng.next_u32();
        while Integer::from(prime).is_probably_prime(15) == IsPrime::No {
            prime = rng.next_u32();
        }

        prime.into()
    }


    #[test]
    fn partials_add_test() {
        let mut a_vals: Vec<u64> = Vec::new();
        let mut b_vals: Vec<u64> = Vec::new();
        let modulus: u64 = rand::thread_rng().gen::<u32>().into();
        for _i in 0..64 {
            a_vals.push(rand::thread_rng().gen::<u64>() % modulus);
            b_vals.push(rand::thread_rng().gen::<u64>() % modulus);
        }
        let a = DoubleCRTPartial { values: a_vals, modulus };
        let b = DoubleCRTPartial { values: b_vals, modulus };
        let result = &a + &b;

        for i in 0..64 {
            assert_eq!(result.values[i], (a.values[i] + b.values[i]) % modulus);
        }
    }

    #[test]
    fn partials_mul_test() {
        let mut a_vals: Vec<u64> = Vec::new();
        let mut b_vals: Vec<u64> = Vec::new();
        let modulus: u64 = rand::thread_rng().gen::<u32>().into();
        let ring = Ring::new(Integer::from(modulus), 6);
        let mut a_vals_rug: Vec<Integer> = Vec::new();
        let mut b_vals_rug: Vec<Integer> = Vec::new();

        for i in 0..64 {
            a_vals.push(rand::thread_rng().gen::<u64>() % modulus);
            a_vals_rug.push(Integer::from(a_vals[i]));
            b_vals.push(rand::thread_rng().gen::<u64>() % modulus);
            b_vals_rug.push(Integer::from(b_vals[i]));
        }

        let a_cmp = RingElement::from(a_vals_rug);
        let b_cmp = RingElement::from(b_vals_rug);

        let a = DoubleCRTPartial { values: a_vals, modulus };
        let b = DoubleCRTPartial { values: b_vals, modulus };

        let result = &a * &b;
        let comparison = ring.mul(&a_cmp, &b_cmp);

        for i in 0..64 {
            assert_eq!(Integer::from(result.values[i]), comparison.values()[i]);
        }

    }

    #[test]
    fn double_crt_completness_test() {
        let mut modulus_products: Vec<u64> = Vec::new();
        let mut modulus = Integer::from(1);
    
        for i in 0..8 {
            modulus_products.push(generate_test_prime());
            modulus = modulus * Integer::from(modulus_products[i]);
        }

        let mut ring = Ring::new(modulus, 6);
        let a = ring.sample();
        let dcrt_a = DoubleCRT::from_ring_element(&a, &modulus_products);
        let a_prime = dcrt_a.to_ring_element();
        assert_eq!(a, a_prime);
    }

    #[test]
    fn double_crt_add_test() {
        let mut modulus_products: Vec<u64> = Vec::new();
        let mut modulus = Integer::from(1);
    
        for i in 0..8 {
            modulus_products.push(generate_test_prime());
            modulus = modulus * Integer::from(modulus_products[i]);
        }

        let mut ring = Ring::new(modulus, 6);
        let a = ring.sample();
        let b = ring.sample();
        let dcrt_a = DoubleCRT::from_ring_element(&a, &modulus_products);
        let dcrt_b = DoubleCRT::from_ring_element(&b, &modulus_products);

        let dcrt_result = &dcrt_a + &dcrt_b;
        let check = ring.add(&a, &b);
        let result = dcrt_result.to_ring_element();

        assert_eq!(result, check);
    }

    #[test]
    fn double_crt_mul_test() {
        let mut modulus_products: Vec<u64> = Vec::new();
        let mut modulus = Integer::from(1);
    
        for i in 0..8 {
            modulus_products.push(generate_test_prime());
            modulus = modulus * Integer::from(modulus_products[i]);
        }

        let mut ring = Ring::new(modulus, 6);
        let a = ring.sample();
        let b = ring.sample();
        let dcrt_a = DoubleCRT::from_ring_element(&a, &modulus_products);
        let dcrt_b = DoubleCRT::from_ring_element(&b, &modulus_products);

        let dcrt_result = &dcrt_a * &dcrt_b;
        let check = ring.mul(&a, &b);
        let result = dcrt_result.to_ring_element();

        assert_eq!(result, check);
    }
}
