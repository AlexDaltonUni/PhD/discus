use crate::cryptosystems::ring::{RingElement, Ring, poly_mul, poly_add};
use serde::{Serialize, Deserialize};
use rug::{
    rand::RandState,
    integer::IsPrime,
    Integer,
    Float,
    ops::Pow
};
use crate::cryptosystems::lfhe::{LFHE, LFHEParameters};
use crate::cryptosystems::dcrt::DoubleCRT;
use std::time::SystemTime;
use std::ops;
use rand::RngCore;

// TODO: refactor RNG to a global generator

//----------------------------------------------------------
// BFV Default Parameters
//----------------------------------------------------------
// TODO: come up with better defaults
pub const LOG_N: u64 = 4;
pub const T_MODULUS: u64 = 16;
pub const B: u64 = 4;
pub const K: u32 = 5;
pub const LEVELS: u32 = 10;

//----------------------------------------------------------
// BFV cryptosystem
//----------------------------------------------------------

// TODO: reimplement this with secure RNG
fn generate_prime(bits: u32) -> Integer {
    let mut rand_state = RandState::new();
    rand_state.seed(&Integer::from(SystemTime::now().elapsed().unwrap().as_secs()));
    let mut prime = Integer::from(Integer::random_bits(bits, &mut rand_state));
    while prime.is_probably_prime(15) == IsPrime::No {
        prime = Integer::from(Integer::random_bits(bits, &mut rand_state));
    }

    prime
}

fn generate_prime_uint() -> u64 {
    let mut rng = rand::thread_rng();
    let mut prime = rng.next_u32();
    while Integer::from(prime).is_probably_prime(15) == IsPrime::No {
        prime = rng.next_u32();
    }
    
    prime.into()
}

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct BFVParameters {
    q_modulus: Integer,     // CipherText Space modulus
    q_factors: Vec<u64>,
    t_modulus: Integer,     // PlainText Space modulus
    k: u32,                 // relinearisation power
    log_n: u64,             // log_2(N) where N is the number of coefficients in R
    b: u64,                 // bound for distribution X
    b_k: Integer,           // bound for distribution X'
    pq: Integer,            // Relinearisation modulus
    p: Integer,             // Relinearisation factor
}

impl BFVParameters {
    pub fn new(t_modulus: Integer, k: u32, log_n: u64, b: u64) -> Self {
        let q_modulus = Integer::from(1);
        let q_factors = Vec::new();
        let b_k = Integer::from(0);
        let pq = Integer::from(0);
        let p = Integer::from(0);
        BFVParameters { q_modulus, q_factors, t_modulus, k, log_n, b, b_k, pq, p }
    }

    pub fn defaults() -> Self {
        let mut params = BFVParameters::new(Integer::from(T_MODULUS), K, LOG_N, B);
        params.set_level(LEVELS);
        params
    }
}

impl LFHEParameters for BFVParameters {
    fn set_level(&mut self, l: u32) {
        let l = l + 10; // approximation error
        // expansion factor: sigma_R = max{||a*b||/(||a||*||b||) : a,b in R}
        // this can be at most root(R.modulus)
        
        // For levels L:
        // floor(q/B) > 4 * sigma_R^L * (sigma_R + 1.25)^(L + 1) * t^(L-1)
        let mut q_bits = 8;
        let sigma_r = self.t_modulus.clone().root(2);
        let target = 4 * sigma_r.clone().pow(l) *
            (Float::with_val(32, 1.25) + sigma_r).pow(l + 1) *
            self.t_modulus.clone().pow(l - 1);

        let mut q_modulus = generate_prime(q_bits);

        while q_modulus.clone().div_rem_floor(Integer::from(self.b)).0 <= target {
            q_bits = q_bits + 1;
            q_modulus = generate_prime(q_bits);
        }

        while self.q_modulus < q_modulus {
            self.q_factors.push(generate_prime_uint());
            self.q_modulus = self.q_modulus.clone() * self.q_factors[self.q_factors.len() - 1];
        }

        let mut pq = Integer::from(1);
        for _i in 0..self.k {
            pq = pq * self.q_modulus.clone();
        }
        self.pq = pq;

        self.b_k = Integer::from(3).root((self.k as f32).sqrt().round() as u32 - 1)
                   * self.q_modulus.clone().pow(self.k - (self.k as f32).sqrt().round() as u32)
                   * self.b.clone().pow((self.k as f32).sqrt().round() as u32);

        let mut p = Integer::from(1);
        for _i in 0..(self.k-1) {
            p = p * self.q_modulus.clone();
        }
        self.p = p;
    }
}

pub struct BFV {
    t_ring: Ring,       // Plaintext space
    q_ring: Ring,       // Ciphertext space
    pq_ring: Ring,      // Relinearisation space
    two_ring: Ring,     // Commonly used R_2
    parameters: BFVParameters
}

impl BFV {
    fn sample_x(&self) -> Integer {
        Ring::new(Integer::from(self.parameters.b), 1).random_coefficient()
    }

    fn sample_x_prime(&self) -> Integer {
        Ring::new(self.parameters.b_k.clone(), 1).random_coefficient()
    }

    pub fn plaintext(&mut self) -> BFVPlainText {
        let mut m = Vec::new();
        for _i in 0..(self.t_ring.order()) { m.push(Integer::from(0)); }
        BFVPlainText { m: RingElement::from(m) }
    }

    pub fn plaintext_from(&mut self, value: u64) -> BFVPlainText {
        let mut plaintext = self.plaintext();
        plaintext.pack(0, value);
        plaintext
    }

    pub fn parameters(&self) -> &BFVParameters { &self.parameters }

    fn relinearisation(&self, c_0: RingElement, c_1: RingElement, c_2: RingElement, rlk: &BFVEvaluationKey) -> (RingElement, RingElement) {
        // Relinearisation
        // c_2_0 = [(c_2 * rlk[0])/p)]_q
        let mut temp = poly_mul(&c_2.values(), &rlk.0.values());
        for i in 0..temp.len() {
            temp[i] = temp[i].clone().div_rem_round(self.parameters.p.clone()).0;
        }
        let c_2_0 = self.q_ring.reduce(&temp);

        // c_2_1 = [(c_2 * rlk[1])/p)]_q
        let mut temp = poly_mul(&c_2.values(), &rlk.1.values());
        for i in 0..temp.len() {
            temp[i] = temp[i].clone().div_rem_round(self.parameters.p.clone()).0;
        }
        let c_2_1 = self.q_ring.reduce(&temp);

        // c = ([c_0 + c_2_0]_q, [c_1 + c_2_1]_q)
        (self.q_ring.add(&c_0, &c_2_0), self.q_ring.add(&c_1, &c_2_1))
    }
}

fn degree_0_re(value: Integer, order: usize) -> RingElement {
    let mut temp = RingElement::from(vec![Integer::from(0); order]);
    temp.set(0, value);
    temp
}

pub type BFVPublicKey = (RingElement, RingElement);
pub type BFVSecretKey = RingElement;
pub type BFVEvaluationKey = (RingElement, RingElement);

#[derive(Clone, Debug, PartialEq)]
pub struct BFVPlainText {
    m: RingElement
}

impl BFVPlainText {
    fn m(&self) -> &RingElement { &self.m }

    pub fn unpack(&self, index: usize) -> Option<u64> { self.m.get(index).to_u64() }

    pub fn pack(&mut self,  index: usize, value: u64,) {
        self.m.set(index, Integer::from(value));
    }
}

impl LFHE for BFV {
    type Parameters = BFVParameters;
    type PlainText = BFVPlainText;
    type CipherText = BFVCipherText;
    type PublicKey = (BFVPublicKey, BFVEvaluationKey);
    type SecretKey = BFVSecretKey;

    fn new(parameters: &Self::Parameters) -> Self {
        let q_ring = Ring::new(parameters.q_modulus.clone(), parameters.log_n);
        let t_ring = Ring::new(parameters.t_modulus.clone(), parameters.log_n);
        let two_ring = Ring::new(Integer::from(2), q_ring.log_n());
        let pq_ring = Ring::new(parameters.pq.clone(), parameters.log_n);

        BFV { t_ring, q_ring, pq_ring, two_ring, parameters: parameters.clone() }
    }

    fn keygen(&mut self) -> Result<(Self::SecretKey, Self::PublicKey), &'static str> {
        // sk <- R_2
        let sk = self.two_ring.sample();

        // a <- R_q, e <- X
        let a = self.q_ring.sample();
        let e = degree_0_re(self.sample_x(), self.q_ring.order());

        // pk = ([-(a * s + e)]_q, a)
        let inner_mul = self.q_ring.mul(&a, &sk);
        let inner_add = self.q_ring.add(&inner_mul, &e);
        let r = self.q_ring.additive_invert(&inner_add);
        let pk = (r, a);

        // a <- R_(pq), e <- X'
        let a_rlk = self.pq_ring.sample();
        let e_rlk = degree_0_re(self.sample_x_prime(), self.pq_ring.order());
        let pss = self.pq_ring.mul(&self.pq_ring.mul(&degree_0_re(self.parameters.p.clone(), self.pq_ring.order()), &sk), &sk);
        let rlk_0 = self.pq_ring.add(&self.pq_ring.additive_invert(&self.pq_ring.add(&self.pq_ring.mul(&a_rlk, &sk), &e_rlk)), &pss);
        let rlk = (rlk_0, a_rlk);

        Ok((sk, (pk, rlk)))
    }

    fn enc(&mut self, m: &Self::PlainText, pk: &Self::PublicKey) -> Result<Self::CipherText, &'static str> {
        // u <- R_2
        // e_1, e_2 <- X
        let u = self.two_ring.sample();
        let e_1 = degree_0_re(self.sample_x(), self.two_ring.order());
        let e_2 = degree_0_re(self.sample_x(), self.two_ring.order());

        // delta = floor(q/t)
        let t = self.t_ring.modulus();
        let delta_value = self.q_ring.modulus().div_rem_floor(t).0;
        let delta = degree_0_re(delta_value, self.q_ring.order());

        // c = ([p_0 * u + e_1 + delta * m]_q, [p_1 * u + e_2]_q)
        let c_1 = self.q_ring.add(&self.q_ring.add(&self.q_ring.mul(&pk.0.0, &u), &e_1), &self.q_ring.mul(&delta, m.m()));
        let c_2 = self.q_ring.add(&self.q_ring.mul(&pk.0.1, &u), &e_2);

        let dcrt_c_1 = DoubleCRT::from_ring_element(&c_1, &self.parameters.q_factors);
        let dcrt_c_2 = DoubleCRT::from_ring_element(&c_2, &self.parameters.q_factors);

        Result::Ok(BFVCipherText { c: (dcrt_c_1, dcrt_c_2), parameters: self.parameters.clone(), pk: pk.clone() })
    }

    fn dec(&self, dcrt_c: &Self::CipherText, sk: &Self::SecretKey) -> Result<Self::PlainText, &'static str> {
        let c = dcrt_c.c_re();
        let t = degree_0_re(self.t_ring.modulus(), self.t_ring.order());
        let scratch = self.q_ring.add(&self.q_ring.mul(&c.1, &sk), &c.0);
        let mut numerator = poly_mul(&t.values(), &scratch.values());

        for i in 0..numerator.len() {
            numerator[i] = numerator[i].clone().div_rem_round(self.q_ring.modulus()).0;
        }

        Result::Ok(BFVPlainText{ m: self.t_ring.reduce(&numerator) })
    }

    fn add(&self, c_1: &Self::CipherText, c_2: &Self::CipherText) -> Result<Self::CipherText, &'static str> {
        let a = &c_1.c().0 + &c_2.c().0;
        let b = &c_1.c().1 + &c_2.c().1;

        Result::Ok(BFVCipherText{ c: (a, b), parameters: self.parameters().clone(), pk: c_1.pk().clone()})
    }

    fn sub(&self, dcrt_c_1: &Self::CipherText, dcrt_c_2: &Self::CipherText) -> Result<Self::CipherText, &'static str> {
        let c_1 = dcrt_c_1.c_re();
        let c_2 = dcrt_c_2.c_re();
        let a = DoubleCRT::from_ring_element(&self.q_ring.add(&c_1.0, &self.q_ring.additive_invert(&c_2.0)), &self.parameters.q_factors);
        let b = DoubleCRT::from_ring_element(&self.q_ring.add(&c_1.1, &self.q_ring.additive_invert(&c_2.1)), &self.parameters.q_factors);

        Result::Ok(BFVCipherText{ c: (a, b), parameters: self.parameters().clone(), pk: dcrt_c_1.pk().clone()})
    }

    fn mul(&self, dcrt_ct_1: &Self::CipherText, dcrt_ct_2: &Self::CipherText, pk: &Self::PublicKey) -> Result<Self::CipherText, &'static str> {
        let ct_1 = dcrt_ct_1.c_re();
        let ct_2 = dcrt_ct_2.c_re();
        // Multiplication
        // c_0 = [(t * (ct_1[0] * ct_2[0]))/q]_q
        let mut temp = poly_mul(&ct_1.0.values(), &ct_2.0.values());
        for i in 0..temp.len() {
            temp[i] = temp[i].clone() * self.t_ring.modulus();
            temp[i] = temp[i].clone().div_rem_round(self.q_ring.modulus()).0;
        }
        let c_0 = self.q_ring.reduce(&temp);

        // c_1 = [(t * (ct_1[0] * ct_2[1] + ct_1[1] * ct_2[0]))/q]_q
        let temp1 = poly_mul(&ct_1.0.values(), &ct_2.1.values());
        let temp2 = poly_mul(&ct_1.1.values(), &ct_2.0.values());
        let mut temp = poly_add(&temp1, &temp2);
        for i in 0..temp.len() {
            temp[i] = temp[i].clone() * self.clone().t_ring.modulus();
            temp[i] = temp[i].clone().div_rem_round(self.q_ring.modulus()).0;
        }
        let c_1 = self.q_ring.reduce(&temp);

        // c_2 = [(t * (ct_1[1] * ct_2[1]))/q]_q
        let mut temp = poly_mul(&ct_1.1.values(), &ct_2.1.values());
        for i in 0..temp.len() {
            temp[i] = temp[i].clone() * self.clone().t_ring.modulus();
            temp[i] = temp[i].clone().div_rem_round(self.q_ring.modulus()).0;
        }
        let c_2 = self.q_ring.reduce(&temp);

        let (a, b) = self.relinearisation(c_0, c_1, c_2, &pk.1);
        let dcrt_a = DoubleCRT::from_ring_element(&a, &self.parameters.q_factors);
        let dcrt_b = DoubleCRT::from_ring_element(&b, &self.parameters.q_factors);
        Result::Ok(BFVCipherText{ c: (dcrt_a, dcrt_b), parameters: self.parameters().clone(), pk: pk.clone()})
    }
}


#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct BFVCipherText {
    c: (DoubleCRT, DoubleCRT),
    parameters: BFVParameters,
    pk: (BFVPublicKey, BFVEvaluationKey),
}

impl BFVCipherText {
    pub fn enc(m: &BFVPlainText, parameters: &BFVParameters, pk: &(BFVPublicKey, BFVEvaluationKey)) -> Result<Self, &'static str> {
        let mut bfv = BFV::new(&parameters);
        bfv.enc(m, pk)
    }

    pub fn dec(&self, sk: &BFVSecretKey) -> Result<BFVPlainText, &'static str> {
        let bfv = BFV::new(&self.parameters);
        bfv.dec(&self, sk)
    }

    fn c(&self) -> &(DoubleCRT, DoubleCRT) { &self.c }

    fn c_re(&self) -> (RingElement, RingElement) {
        (self.c.0.to_ring_element(), self.c.1.to_ring_element())
    }

    pub fn parameters(&self) -> &BFVParameters { &self.parameters }
    pub fn pk(&self) -> &(BFVPublicKey, BFVEvaluationKey) { &self.pk }
}

impl ops::Add<&BFVCipherText> for &BFVCipherText {
    type Output = BFVCipherText;
    fn add(self, rhs: &BFVCipherText) -> BFVCipherText {
        let bfv = BFV::new(&self.parameters);
        bfv.add(self, rhs).unwrap()
    }
}

impl ops::Sub<&BFVCipherText> for &BFVCipherText {
    type Output = BFVCipherText;
    fn sub(self, rhs: &BFVCipherText) -> BFVCipherText {
        let bfv = BFV::new(&self.parameters);
        bfv.sub(self, rhs).unwrap()
    }
}

impl ops::Mul<&BFVCipherText> for &BFVCipherText {
    type Output = BFVCipherText;
    fn mul(self, rhs: &BFVCipherText) -> BFVCipherText {
        let bfv = BFV::new(&self.parameters);
        bfv.mul(self, rhs, &self.pk).unwrap()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    fn test_parameters() -> BFVParameters {
        BFVParameters::defaults()
    }

    fn random_plaintext(param: BFVParameters) -> BFVPlainText {
        let mut ring = Ring::new(param.t_modulus, param.log_n);
        BFVPlainText{ m: ring.sample() }
    }

    #[test]
    fn completeness_test() {
        let parameters = test_parameters();
        let mut bfv = BFV::new(&parameters);
        let (sk, pk) = bfv.keygen().unwrap();
        println!("pk: {:?}", pk);
        println!("sk: {:?}", sk);
        let m = random_plaintext(parameters);
        println!("m: {:?}", m);
        let c = bfv.enc(&m, &pk).unwrap();
        println!("c: {:?}", c);
        let m_prime = bfv.dec(&c, &sk).unwrap();
        println!("m': {:?}", m_prime);
        assert_eq!(m.m(), m_prime.m());
    }

    #[test]
    fn add_test() {
        let parameters = test_parameters();
        let mut bfv = BFV::new(&parameters);
        let (sk, pk) = bfv.keygen().unwrap();
        let m_1 = random_plaintext(parameters.clone());
        let m_2 = random_plaintext(parameters.clone());
        let c_1 = bfv.enc(&m_1, &pk).unwrap();
        let c_2 = bfv.enc(&m_2, &pk).unwrap();
        let ring = Ring::new(parameters.t_modulus, parameters.log_n);
        let check = ring.add(m_1.m(), m_2.m());
        let result = bfv.dec(&bfv.add(&c_1, &c_2).unwrap(), &sk).unwrap();
        assert_eq!(check, *result.m());
    }

    #[test]
    fn mul_test() {
        let parameters = test_parameters();
        let mut bfv = BFV::new(&parameters);
        let (sk, pk) = bfv.keygen().unwrap();
        let m_1 = random_plaintext(parameters.clone());
        let m_2 = random_plaintext(parameters.clone());
        let c_1 = bfv.enc(&m_1, &pk).unwrap();
        let c_2 = bfv.enc(&m_2, &pk).unwrap();
        let ring = Ring::new(parameters.t_modulus, parameters.log_n);
        let check = ring.mul(m_1.m(), m_2.m());
        let result = bfv.dec(&bfv.mul(&c_1, &c_2, &pk).unwrap(), &sk).unwrap();
        assert_eq!(check, *result.m());
    }

    #[test]
    fn levels_test() {
        let parameters = test_parameters();
        let mut bfv = BFV::new(&parameters);
        let (sk, pk) = bfv.keygen().unwrap();
        let m_1 = random_plaintext(parameters.clone());
        let m_2 = random_plaintext(parameters.clone());
        let c_1 = bfv.enc(&m_1, &pk).unwrap();
        let c_2 = bfv.enc(&m_2, &pk).unwrap();
        let mut result = bfv.mul(&c_1, &c_2, &pk).unwrap();
        let ring = Ring::new(parameters.t_modulus, parameters.log_n);
        let mut check = ring.mul(m_1.m(), m_2.m());

        for _i in 0..LEVELS {
            result = bfv.mul(&result.clone(), &c_2, &pk).unwrap();
            check = ring.mul(&check.clone(), &m_2.m());
        }

        assert_eq!(check, *bfv.dec(&result, &sk).unwrap().m());
    }

    #[test]
    fn packing_plaintexts_test() {
        let parameters = test_parameters();
        let mut bfv = BFV::new(&parameters);
        let (sk, pk) = bfv.keygen().unwrap();
        let mut all_m = Vec::new();
        let mut m = bfv.plaintext();
        for i in 0..(1 << LOG_N) {
            let m_i = bfv.t_ring.random_coefficient().to_u64().unwrap();
            all_m.push(m_i);
            m.pack(i, m_i);
        }
        let c = bfv.enc(&m, &pk).unwrap();
        let m_prime = bfv.dec(&c, &sk).unwrap();
        assert_eq!(m.m(), m_prime.m());
    }
}
