pub trait LFHEParameters {
    fn set_level(&mut self, l: u32);
}

pub trait LFHE {
    type Parameters;
    type PlainText;
    type CipherText;
    type PublicKey;
    type SecretKey;

    fn new(parameters: &Self::Parameters) -> Self;
    fn keygen(&mut self) -> Result<(Self::SecretKey, Self::PublicKey), &'static str>;
    fn enc(&mut self, m: &Self::PlainText, pk: &Self::PublicKey) -> Result<Self::CipherText, &'static str>;
    fn dec(&self, c: &Self::CipherText, sk: &Self::SecretKey) -> Result<Self::PlainText, &'static str>;
    fn add(&self, c_1: &Self::CipherText, c_2: &Self::CipherText) -> Result<Self::CipherText, &'static str>;
    fn sub(&self, c_1: &Self::CipherText, c_2: &Self::CipherText) -> Result<Self::CipherText, &'static str>;
    fn mul(&self, c_1: &Self::CipherText, c_2: &Self::CipherText, pk: &Self::PublicKey) -> Result<Self::CipherText, &'static str>;
}
