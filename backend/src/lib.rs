mod cryptosystems;
mod fragmenter;
mod interpreter;

pub use cryptosystems::ring::Ring;
pub use cryptosystems::bfv::BFVCipherText;
pub use cryptosystems::lfhe::{LFHE, LFHEParameters};
pub use fragmenter::{GlobalInstruction, Operation, Data, State, Operand, Fragment, FragmentID, Program};

pub fn interpret(program: &mut Program) -> Result<(), &str> {
    interpreter::interpret(program)
}

pub fn write_program(program: Vec<GlobalInstruction>, file_name: String) -> Result<(), &'static str> {
    fragmenter::write_program(program, file_name)
}
pub fn read_program(file_name: String) -> Result<Vec<GlobalInstruction>, &'static str> {
    fragmenter::read_program(file_name)
}
pub fn execute_fragment(fragment: &mut Fragment) -> Result<(), &str> {
    interpreter::execute_fragment(fragment)
}

pub fn propegate_results(index: usize, program: &mut Program) -> Result<(), &'static str> {
    interpreter::propegate_results(index, program)
}

pub fn replace_operands(fragment: &mut Fragment, operands: &Vec<Operand>) -> Result<(), &'static str> {
    interpreter::replace_operands(fragment, operands)
}
