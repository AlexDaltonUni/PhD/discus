use async_std::{io, task};
use log::{error, info};
use once_cell::sync::Lazy;
use std::{task::{Context, Poll}, error::Error};
use libp2p::{
    Multiaddr,
    PeerId,
    Swarm,
    identity,
    swarm::{SwarmEvent, SwarmBuilder},
};
use futures::prelude::*;
use crate::processing::{
    process_work_request,
    handle_list_programs,
    handle_list_peers,
    handle_run_program,
    benchmark,
};
use crate::behaviour::{NetworkEvent, DSCSBehaviour};

static KEYS: Lazy<identity::Keypair> = Lazy::new(|| identity::Keypair::generate_ed25519());
static PEER_ID: Lazy<PeerId> = Lazy::new(|| PeerId::from(KEYS.public()));

async fn create_swarm() -> Result<Swarm<DSCSBehaviour>, Box<dyn Error>> {
    let transport = libp2p::development_transport(KEYS.clone()).await?;
    let behaviour = DSCSBehaviour::new();
    info!("Peer ID: {}", &PEER_ID.clone());
    let swarm = SwarmBuilder::new(transport, behaviour, PEER_ID.clone())
        .executor(Box::new(|fut| {
            task::spawn(fut);
        }))
    .build();
    Ok(swarm)
}

pub async fn connect(remote_addr: Option<Multiaddr>) -> Result<(), Box<dyn Error>> {
    let mut swarm = create_swarm().await?;
    let mut stdin = io::BufReader::new(io::stdin()).lines();

    Swarm::listen_on(&mut swarm, "/ip4/0.0.0.0/tcp/0".parse()?)?;

    if let Some(addr) = remote_addr {
        Swarm::dial_addr(&mut swarm, addr.clone())?;
        println!("Dialed {:?}", addr)
    }
    
    task::block_on(future::poll_fn(move |cx: &mut Context<'_>| {
        loop {
            match stdin.try_poll_next_unpin(cx)? {
                Poll::Ready(Some(line)) => {
                    match line.as_str() {
                        "ls p" => task::block_on(handle_list_peers(&mut swarm)),
                        "ls stack" => println!("{:?}", swarm.behaviour().events),
                        cmd if cmd.starts_with("ls") => task::block_on(handle_list_programs()),
                        cmd if cmd.starts_with("run") => {
                            let queue = swarm.behaviour().request_queue.clone();
                            task::spawn(handle_run_program(queue, String::from(&cmd[4..])));
                        },
                        cmd if cmd.starts_with("benchmark") => {
                            let queue = swarm.behaviour().request_queue.clone();
                            task::spawn(benchmark(queue));
                        }
                        cmd if cmd.starts_with("exit") => return Poll::Ready(Result::Ok(())),
                        _ => error!("unknown command"),
                    }
                },
                Poll::Ready(None) => panic!("stdin closed"),
                Poll::Pending => break,
            }
        }

        loop {
            match swarm.poll_next_unpin(cx) {
                Poll::Ready(Some(SwarmEvent::NewListenAddr{ address, .. })) => {
                    println!("Listening on {:?}", address);
                },
                Poll::Ready(Some(SwarmEvent::Behaviour(e))) => {
                    match e {
                        NetworkEvent::Request(pid, channel, mut request) => {
                            debug!("DSCS request from peer {}", pid);
                            trace!("DSCS request: [peer: {}, request: {:?}]", pid, request);
                            swarm.behaviour_mut().rr.send_response(channel, task::block_on(process_work_request(&mut request))).unwrap();
                        },
                        NetworkEvent::Response(pid, rid, response) => {
                            debug!("DSCS response to peer {}", pid);
                            trace!("DSCS response: [peer: {}, id: {}, response: {:?}]", pid, rid, response);
                        },
                        NetworkEvent::NewPeer(pid) => {
                            debug!("DSCS new peer: [peer: {}]", pid);
                        },
                        NetworkEvent::LostPeer(pid) => {
                            debug!("DSCS lost peer: [peer: {}]", pid);
                        },
                        NetworkEvent::PendingOutboundRequest(request, state) => {
                            swarm.behaviour_mut().work_request(request, state);
                        }
                    }
                },
                Poll::Ready(None) => return Poll::Ready(Result::Ok(())),
                Poll::Pending => break,
                _ => error!("Unexpected pattern poll next unpin"),
            }
        }
        Poll::Pending
    }))
}
