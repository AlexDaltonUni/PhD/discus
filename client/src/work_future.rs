use async_std::task::{Context, Poll, Waker};
use crate::protocol::WorkResponse;
use std::{
    sync::{Arc, Mutex},
    pin::Pin,
};
use futures::Future;
use discus_backend::{
    Operand,
    Fragment,
    replace_operands
};

#[derive(Debug)]
pub struct WorkSharedState {
    completed: bool,
    result: Option<WorkResponse>,
    waker: Option<Waker>,
}

impl WorkSharedState {
    pub fn done(&mut self, result: WorkResponse) {
        self.completed = true;
        self.result = Some(result);
        if let Some(waker) = self.waker.take() {
            waker.wake();
        }
    }
}

pub struct WorkResponseFuture {
    pub shared_state: Arc<Mutex<WorkSharedState>>,
}

impl WorkResponseFuture {
    pub fn new() -> Self {
        let shared_state = Arc::new(Mutex::new(WorkSharedState {
            completed: false,
            result: None,
            waker: None,
        }));
        WorkResponseFuture { shared_state }
    }
}

impl Future for WorkResponseFuture {
    type Output = WorkResponse;

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let mut state = self.shared_state.lock().unwrap();
        if state.completed {
            debug!("Future polled: Ready");
            Poll::Ready(state.result.as_ref().unwrap().clone())
        } else {
            debug!("Future polled: Not ready");
            state.waker = Some(cx.waker().clone());
            Poll::Pending
        }
    }
}

#[derive(Debug)]
pub struct FragmentSharedState {
    pub fragment: Fragment,
    waker: Option<Waker>,
}

impl FragmentSharedState {
    pub fn new(fragment: Fragment) -> Self {
        FragmentSharedState { fragment, waker: None }
    }

    pub fn update(&mut self, operands: &Vec<Operand>) {
        replace_operands(&mut self.fragment, operands).unwrap();
        if let Some(waker) = self.waker.take() {
            waker.wake();
        }
    }
}

pub struct FragmentReadyFuture {
    pub shared_state: Arc<Mutex<FragmentSharedState>>,
}

impl FragmentReadyFuture {
    pub fn new(shared_state: Arc<Mutex<FragmentSharedState>>) -> Self {
        FragmentReadyFuture{ shared_state }
    }
}

impl Future for FragmentReadyFuture {
    type Output = Arc<Mutex<FragmentSharedState>>;

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let mut shared_state_lock = self.shared_state.lock().unwrap();
        if shared_state_lock.fragment.ready() {
            Poll::Ready(self.shared_state.clone())
        } else {
            shared_state_lock.waker = Some(cx.waker().clone());
            Poll::Pending
        }
    }
}
