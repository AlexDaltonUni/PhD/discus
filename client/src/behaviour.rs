use libp2p::{
    PeerId,
    request_response::{RequestId, ResponseChannel, RequestResponseEvent, RequestResponseMessage, RequestResponseConfig, ProtocolSupport, RequestResponse},
    swarm::{NetworkBehaviourEventProcess, NetworkBehaviourAction, PollParameters},
    mdns::{Mdns, MdnsConfig, MdnsEvent},
    NetworkBehaviour,
};
use async_std::{task, task::{Context, Poll}};
use log::error;
use std::{
    iter,
    collections::{HashSet, VecDeque, HashMap},
    sync::{Arc, Mutex},
};
use crate::protocol::{DSCSProtocol, DSCSProtocolCodec, WorkRequest, WorkResponse};
use crate::work_future::WorkSharedState;

#[derive(Debug)]
pub enum NetworkEvent {
    Request(PeerId, ResponseChannel<WorkResponse>, WorkRequest),
    Response(PeerId, RequestId, WorkResponse),
    NewPeer(PeerId),
    LostPeer(PeerId),
    PendingOutboundRequest(WorkRequest, Arc<Mutex<WorkSharedState>>),
}

type RequestQueue = Arc<Mutex<Vec<(WorkRequest, Arc<Mutex<WorkSharedState>>)>>>;

#[derive(NetworkBehaviour)]
#[behaviour(out_event = "NetworkEvent", poll_method = "poll")]
pub struct DSCSBehaviour {
    pub mdns: Mdns,
    pub rr: RequestResponse<DSCSProtocolCodec>,

    #[behaviour(ignore)]
    pub peers: Mutex<HashSet<PeerId>>,
    #[behaviour(ignore)]
    peers_queue: VecDeque<PeerId>,
    #[behaviour(ignore)]
    pub events: Vec<NetworkEvent>,
    #[behaviour(ignore)]
    id_states: HashMap<RequestId, Arc<Mutex<WorkSharedState>>>,
    #[behaviour(ignore)]
    pub request_queue: RequestQueue,
}

impl NetworkBehaviourEventProcess<MdnsEvent> for DSCSBehaviour {
    fn inject_event(&mut self, event: MdnsEvent) {
        let mut peers = self.peers.lock().unwrap();
        match event {
            MdnsEvent::Discovered(discovered_list) => {
                for (peer, _) in discovered_list {
                    self.events.push(NetworkEvent::NewPeer(peer));
                    peers.insert(peer);
                    self.peers_queue.push_front(peer);
                    debug!("Peer added {:?}", peer);
                }
            }
            MdnsEvent::Expired(expired_list) => {
                for (peer, _) in expired_list {
                    if !self.mdns.has_node(&peer) {
                        self.events.push(NetworkEvent::LostPeer(peer));
                        peers.remove(&peer);
                        debug!("Peer removed {:?}", peer);
                    }
                }
            }
        }
    }
}

impl NetworkBehaviourEventProcess<RequestResponseEvent<WorkRequest, WorkResponse>> for DSCSBehaviour {
    fn inject_event(&mut self, event: RequestResponseEvent<WorkRequest, WorkResponse>) {
        match event {
            RequestResponseEvent::Message { peer, message } => {
                match message {
                    RequestResponseMessage::Request { request_id, request, channel } => {
                        self.events.push( NetworkEvent::Request(peer, channel, request.clone()) );
                        trace!("DSCS Request: [peer: {}, request_id: {}, request {:?}]", peer, request_id, request);
                    }
                    RequestResponseMessage::Response { request_id, response } => {
                        self.events.push( NetworkEvent::Response(peer, request_id, response.clone()) );
                        trace!("DSCS Response: [peer: {}, request_id: {}, response {:?}]", peer, request_id, response);
                        if let Some(future) = self.id_states.remove(&request_id) {
                            future.lock().unwrap().done(response)
                        }
                    }
                }
            },
            RequestResponseEvent::OutboundFailure { peer, request_id, error } => {
                error!("Outbound failure: [peer: {}, id: {}, error: {:?}]", peer, request_id, error);
            },
            RequestResponseEvent::InboundFailure { peer, request_id, error } => {
                error!("Inbound failure: [peer: {}, id: {}, error: {:?}]", peer, request_id, error);
            },
            RequestResponseEvent::ResponseSent { peer, request_id } => {
                debug!("Reponse sent: [peer: {}, id: {}]", peer, request_id);
            }
        }
    }
}

impl DSCSBehaviour {
    pub fn new() -> Self {
        let protocols = iter::once((DSCSProtocol, ProtocolSupport::Full));
        let cfg = RequestResponseConfig::default();
        let rr = RequestResponse::new(DSCSProtocolCodec, protocols, cfg);
        let mdns = task::block_on(Mdns::new(MdnsConfig::default()))
            .expect("unable to construct Mdns protocol handler");
        DSCSBehaviour {
            mdns,
            rr,
            peers: Mutex::new(HashSet::new()),
            events: Vec::new(),
            peers_queue: VecDeque::new(),
            id_states: HashMap::new(),
            request_queue: Arc::new(Mutex::new(Vec::new())),
        }
    }

    fn get_next_peer(&mut self) -> Result<PeerId, &'static str> {
        match self.peers_queue.pop_front() {
            Some(id) => {
                let contained = {
                    let peers = self.peers.lock().unwrap();
                    peers.contains(&id)
                };
                if !contained {
                    self.get_next_peer()
                } else {
                    self.peers_queue.push_back(id);
                    Result::Ok(id.clone())
                }
            },
            None => Result::Err("no peers in queue")
        }
    }

    fn poll<TBehaviourIn>(&mut self, _: &mut Context, _: &mut impl PollParameters)
        -> Poll<NetworkBehaviourAction<TBehaviourIn, NetworkEvent>>
    {
        if !self.events.is_empty() {
            return Poll::Ready(NetworkBehaviourAction::GenerateEvent(self.events.remove(0)));
        }
        let mut request_queue_lock = self.request_queue.lock().unwrap();
        if !request_queue_lock.is_empty() {
            let (request, state) = request_queue_lock.remove(0);
            let event = NetworkEvent::PendingOutboundRequest(request, state);
            return Poll::Ready(NetworkBehaviourAction::GenerateEvent(event));
        }
        Poll::Pending
    }

    pub fn work_request(&mut self, request: WorkRequest, future_state: Arc<Mutex<WorkSharedState>>) {
        let peer = self.get_next_peer().unwrap();
        let id = self.rr.send_request(&peer, request);
        self.id_states.insert(id, future_state.clone());
    }
}
