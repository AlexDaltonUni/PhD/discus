use serde::{Deserialize, Serialize};
use serde_json::{from_slice, to_vec};
use discus_backend::{Operand, Fragment, FragmentID};
use async_trait::async_trait;
use libp2p::{
    core::ProtocolName,
    request_response::RequestResponseCodec,
};
use std::io;
use futures::{
    prelude::*,
    io::{AsyncRead, AsyncReadExt, AsyncWrite},
};

const PROTOCOL_NAME: &[u8] = b"/dscs/0.0.1";

#[derive(Clone)]
pub struct DSCSProtocol;

impl ProtocolName for DSCSProtocol {
    fn protocol_name(&self) -> &[u8] {
        PROTOCOL_NAME
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct WorkRequest {
    pub fragment: Fragment,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct WorkResponse {
    pub id: FragmentID,
    pub results: Vec<Operand>,
}

#[derive(Clone)]
pub struct DSCSProtocolCodec;

#[async_trait]
impl RequestResponseCodec for DSCSProtocolCodec {
    type Protocol = DSCSProtocol;
    type Request = WorkRequest;
    type Response = WorkResponse;

    async fn read_request<T>(&mut self, _: &DSCSProtocol, io: &mut T) -> io::Result<Self::Request>
        where
        T: AsyncRead + Unpin + Send,
        {
            let mut buffer = Vec::new();
            io.read_to_end(&mut buffer).await?;
            Ok(from_slice(&buffer)?)
        }

    async fn read_response<T>(&mut self, _: &DSCSProtocol, io: &mut T) -> io::Result<Self::Response>
        where
        T: AsyncRead + Unpin + Send,
        {
            let mut buffer = Vec::new();
            io.read_to_end(&mut buffer).await?;
            Ok(from_slice(&buffer)?)
        }

    async fn write_request<T>(&mut self, _: &DSCSProtocol, io: &mut T, req: Self::Request) -> io::Result<()>
        where
        T: AsyncWrite + Unpin + Send,
    {
        io.write_all(&to_vec(&req)?).await
    }

    async fn write_response<T>(&mut self, _: &DSCSProtocol, io: &mut T, res: Self::Response) -> io::Result<()>
        where
        T: AsyncWrite + Unpin + Send,
    {
        io.write_all(&to_vec(&res)?).await
    }
}
