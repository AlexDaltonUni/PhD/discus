# DiSCuS

DiSCuS is a framework implementing the DSCS distributed secure computation
protocol written in Rust. It includes:
* libp2p distributed networking
* accelerated homomorphic encryption

## Getting Started

Install rust
```
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

Install DiSCuS

```
git clone https://gitlab.com/AlexDaltonPhD/discus.git
cd discus/client
cargo install
```

## Writing DiSCuS Programs

Currently programs are defined programmatically as rust structures. To write a
DiSCuS program one must create a function in Rust which returns an appropriate
data structure. These can be serialised and deserialised for convenience.

## Disclaimer

DiSCuS is **NOT** ready for deployment as anything other than a curiosity. It is
written by a single student prioritising functionality and the ability to gather
data rather than complete application security. As such there are a great deal
of changes that need to be made before it is deployed. These include things like
using true randomness to seed PRNGs among other things. The full attack surface
against this program is unknown.

There are more attack vectors covered in the publication accompanying this
application.
